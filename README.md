# Parser for WebMagic test task #

**Parse the URL as [required](https://bitbucket.org/webmagic/testing/src/master/TestTask.md) by the test task**

This package parse posts from selected URL, saving it in database with checkin for consisting there

* Version 1.0
-----
* [Required environment](#required-environment)
* [Installation](#installation)
* [Database configuration](#database-configuration)
* [Usage](#usage)

-----


## Required environment

* 300 Mb of disk space
* Server [environment](https://laravel.com/docs/6.x/installation#server-requirements) for Laravel 6.*
* Global installed [composer](https://getcomposer.org/)
* Global installed [laravel/installer](https://laravel.com/docs/6.x/installation#installing-laravel)
* internet connection


## Installation

1. Clone [repository](https://bitbucket.org/AKaramanec/webmagictask/src/master/) from [Bitbacket](https://confluence.atlassian.com/bitbucket/clone-a-repository-223217891.html)
2. Run in console `conposer install`, wait for composer install, thane run `npm install && npm dev run` to npm install
3. Make sure that the server is running the database available
4. Run `cp .env.example .env` to make env file
5. Open env file and in APP section configure application like this::
```dotenv
APP_NAME=Parser
APP_ENV=local
APP_KEY=base64:Pwkn9Us1dGOhOPeN84CIXJ++695T4qV+8ixJGUNoJ4k=
APP_DEBUG=true
APP_URL=http://localhost
``` 


## Database configuration

6. Open env file and go to DB section and [configure](https://laravel.com/docs/6.x/database) connection.
    In my case it look like this:
```dotenv
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=webmagic_task
DB_USERNAME=root
DB_PASSWORD=
``` 


## Usage

* To start usage first need make migrations so run `php artisan migrate`
* Than [run](https://laravel.com/docs/6.x#installing-laravel) web server, so run `php artisan serve` in console
* Open browser and go to `http://127.0.0.1:8000` address
![Image of Yaktocat](./public/img/md/home_page.png)
* By navigate menu, go to the Parser or Posts page:
![Image of Yaktocat](./public/img/md/pan_menu.png)
* On Parser page click 'PARSE' bottom, parse process run than you well redirect to Posts page
![Image of Yaktocat](./public/img/md/parse.png)
![Image of Yaktocat](./public/img/md/posts.png)

## DONE!
