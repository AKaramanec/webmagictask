<?php

namespace App\Console\Commands;

use App\Helpers\ParsingUrl;
use Illuminate\Console\Command;

class ParserCoomand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parse:run
                            {url=https://www.segodnya.ua/regions/odessa.html : URL what need to parse}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the parse of url';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @param ParsingUrl $parsing_url
     * @return mixed
     * @throws \Exception
     */
    public function handle(ParsingUrl $parsing_url)
    {
        $url = $this->argument('url');

        try {
            $parsing_url->parse_data($url);
        } catch (Exception $exception) {
            $this->info("<error>Oops, something went wrong</error>");
            throw $exception;
        }

        $this->info("<info>Parsing of {$url} done</info>");
    }
}
