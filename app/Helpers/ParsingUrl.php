<?php

namespace App\Helpers;

use App\Models\Author;
use App\Models\Post;
use App\Models\Url;
use DateTime;
use Illuminate\Database\Eloquent\Model;
use \KubAT\PhpSimple\HtmlDomParser;

class ParsingUrl
{
    /**
     * @var
     */
    protected $parent_url_id;

    /**
     *
     *
     * @param string $url
     * @param string $referer
     * @return mixed
     */
    public function get_html_dom(string $url, string $referer = 'https://www.google.com/')
    {
        $curl = $this->curl_create($url, $referer);
        return $this->create_html_dom($curl);
    }
    /**
     * Create connection by cURL
     *
     * @param string $url
     * @param string $referer
     * @return bool|string
     */
    public function curl_create(string $url, string $referer)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36");
        curl_setopt($ch, CURLOPT_REFERER, $referer);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $data = curl_exec($ch);
        curl_close($ch);

        return $data;
    }

    /**
     * Generate DOM by the Simple HTML dom
     *
     * @param string $html_doc
     * @return mixed
     */
    public function create_html_dom(string $html_doc)
    {
        return HtmlDomParser::str_get_html($html_doc);
    }

    /**
     * Parse data from open URL
     *
     * @param string $url
     * @throws \Exception
     */
    public function parse_data(string $url = 'https://www.segodnya.ua/regions/odessa.html')
    {
        $now = new DateTime();

        // Add URL to database if it added yet
        $url_model = $this->add_url_to_bd($url,'base url', 0);

        $dom = $this->get_html_dom($url);
        $elements = $dom->find('.st__news-list li');
        $this->parent_url_id = $url_model->id;

        foreach ($elements as $element) {
            // Get info about link
            $href = $element->find('a', 0)->href;
            $slug = $this->slug_create($href);
            $timestamp = (int)($element->find('span.time.timestamp-date', 0)->getAttribute('data-timestamp'));

            // Check post publication date for entry into the date range
            if (!$this->is_in_date_range($timestamp, $now, 5)) continue;

            // Add URL to database if it added yet
            $url_model = $this->add_url_to_bd($href, $slug, $this->parent_url_id);

            // Parse URL if it not parsed yet
            if ($url_model->parsed_at === null) {
                $this->open_and_parse_post($url_model, $slug, $now);
            }
        }
    }

    /**
     * Add URL to database if it added yet
     *
     * @param string $url
     * @param string $description
     * @param int $parent_url_id
     * @return Url
     */
    public function add_url_to_bd(string $url, string $description, int $parent_url_id)
    {
        // Check the URL in database
        $url_model = (new Url())->where('url', $url)->first();

        // Add URL to database
        if (is_null($url_model)) {
            $url_model = new Url();
            $url_model->url = $url;
            $url_model->parent_id = $parent_url_id;
            $url_model->description = $description;
            $url_model->save();
        }

        return $url_model;
    }

    /**
     * Check the Author in the database if not, add
     *
     * @param string $name
     * @return Author
     */
    public function add_author_to_bd(string $name)
    {
        // Check the Author in database
        $url_author = (new Author())->where('name', $name)->firstOr(function () {
            return false;
        });

        // Add Author to database
        if (!$url_author) {
            $url_author = new Author();
            $url_author->name = $name;
            $url_author->save();
        }

        return $url_author;
    }

    /**
     * Open post by URL and parse data from it.
     * Mark URL as parsed
     *
     * @param Model $url_model
     * @param string $slug
     * @param DateTime $now
     */
    public function open_and_parse_post(Model $url_model, string $slug,DateTime $now)
    {
        // data collection
        $dom = $this->get_html_dom($url_model->url);
        $keywords = $dom->find('[name="keywords"]', 0)->getAttribute('content');
        $json_ld = json_decode($dom->find('[type="application/ld+json"]', 0)->innertext());
        $author_model = $this->add_author_to_bd($json_ld->author->name);

        // Add post to database
        $post_model = new Post();
        $post_model->author_id = $author_model->id;
        $post_model->url_id = $url_model->id;
        $post_model->slug = $slug;
        $post_model->title = $json_ld->headline;
        $post_model->content = $json_ld->articleBody;
        $post_model->keywords = $keywords;
        $post_model->published_at = date('y-m-d H:i:s', strtotime($json_ld->datePublished));
        $post_model->save();

        // Mark URL as parsed
        $url_model->parsed_at = $now;
        $url_model->save();
    }

    /**
     * Generate slug from URL
     *
     * @param string $href
     * @return string
     */
    public function slug_create(string $href)
    {
        $slug = basename(parse_url($href, PHP_URL_PATH));
        $slug = substr($slug, 0, -13);

        if (is_string($trim = stristr($slug, '-foto-i-video'))) {
            return $this->trim_slug($slug, $trim);
        };

        if (is_string($trim = stristr($slug, '-foto'))) {
            return $this->trim_slug($slug, $trim);
        };

        if (is_string($trim = stristr($slug, '-video'))) {
            return $this->trim_slug($slug, $trim);
        };

        return $slug;
    }

    /**
     *  Trim keywords like '-foto-i-video' from slug
     *
     * @param string $str
     * @param string $trim
     * @return string|string[]|null
     */
    public function trim_slug(string $str, string $trim)
    {
        $trim = '~' . $trim . '~';
        return preg_replace("{$trim}", '', $str);
    }

    /**
     * Check post publication date for entry into the date range
     *
     * @param DateTime $timestamp
     * @param DateTime $now
     * @param $day_range
     * @return bool
     */
    public function is_in_date_range(int $timestamp, DateTime $now, $day_range)
    {
        $date = DateTime::createFromFormat("H:i d.m.Y", date('H:i d.m.Y', $timestamp))->setTime(0,0,0);

        if ($now->diff($date)->d <= $day_range) {
            return true;
        }

        return false;
    }
}
