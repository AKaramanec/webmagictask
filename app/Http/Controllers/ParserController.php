<?php

namespace App\Http\Controllers;

use App\Helpers\ParsingUrl;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;
use Illuminate\View\View;

class ParserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        return view('parser');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $parsing_url = new ParsingUrl();
        $parsing_url->parse_data($request->url);

        return redirect()->route('posts.index');
    }
}
