<?php

namespace App\Http\Controllers;

use App\Repositories\PostRepositories;
use Illuminate\Contracts\View\Factory;
use Illuminate\View\View;

class PostController extends Controller
{
    /**
     * @var PostRepositories
     */
    private $post_repositories;

    /**
     * PostController constructor.
     */
    public function __construct()
    {
        $this->post_repositories = app(PostRepositories::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     * @throws \Exception
     */
    public function index()
    {
        $post_paginator = $this->post_repositories->getPostsForTable();

        return view('posts', compact('post_paginator'));
    }
}
