<?php

namespace App\Repositories;

use App\Models\Post as Model;
use Illuminate\Pagination\Paginator;

/**
 * Repository for working with an entity Post
 * Collecting and providing information by entity
 *
 * Class PostRepositories
 *
 * @package App\Repositories
 */
class PostRepositories extends CoreRepositories
{
    /**
     * @return string
     */
    public function getModelClass()
    {
        return Model::class;
    }

    /**
     * @param int $day_range
     * @return Paginator
     * @throws \Exception
     */
    public function getPostsForTable(int $day_range = 5)
    {
        $columns = ['author_id', 'url_id', 'title', 'keywords', 'published_at'];

        return $this->startCondition()->select($columns)
            ->with('author:id,name', 'url:id,url')
            ->whereDate('published_at', '>=', now()->subDays($day_range)->setTime(0, 0, 0)->toDateTimeString())
            ->get();
    }
}
