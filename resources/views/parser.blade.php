@extends('layout')

@section('content')
    <div class="container">
        <form method="POST" action="{{ route('parser.store') }}">
            @csrf
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            Needs to parse?
                        </div>
                        <div class="card-body">
                            <div class="card-title"></div>
                            <div class="card-subtitle mb-2 text-muted"></div>
                            <div class="form-group">
                                <label for="url">Parsing URL</label>
                                <input name="url"
                                       value="https://www.segodnya.ua/regions/odessa.html"
                                       id="url"
                                       type="text"
                                       class="form-control"
                                       minlength="3"
                                       required>
                            </div>
                            <button type="submit" class="btn btn-primary">Parse</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection
