@extends('layout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <table id="dtBasicExample" class="table table-striped table-bordered table-sm"
                               cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th class="th-sm">Published
                                </th>
                                <th class="th-sm">Title
                                </th>
                                <th class="th-sm">Author
                                </th>
                                <th class="th-sm">Keywords
                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($post_paginator as $paginator_page)
                                <tr>
                                    <td>{{ date('H:i d.m.Y', strtotime($paginator_page->published_at)) }}</td>
                                    <td>
                                        <a href="{{ $paginator_page->url->url }}"> {{ $paginator_page->title }} </a>
                                    </td>
                                    <td>{{ $paginator_page->author->name }}</td>
                                    <td>{{ $paginator_page->keywords }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                            <tr>
                                <th class="th-sm">Published
                                </th>
                                <th class="th-sm">Title
                                </th>
                                <th class="th-sm">Author
                                </th>
                                <th class="th-sm">Keywords
                                </th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- table option script -->
    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $('#dtBasicExample').DataTable({
                "order": [[2, "desc"]],
            });
            $('.dataTables_length').addClass('bs-select');
        });
    </script>
@endsection
