@extends('layout')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3>
                            Тестовое задание на должность Laravel разработчика
                        </h3>
                    </div>
                    <div class="card-body">
                        <div class="row justify-content-center">
                            <h4>
                                Необходимо разработать базовый функционал новостного пасрера. Логика работы следующая:
                            </h4>
                            <table class="table table-hover">
                                <thead>
                                <tr>
                                    <th>Steps</th>
                                    <th>Description</th>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        1.
                                    </td>
                                    <td>
                                        с сайта https://www.segodnya.ua для региона Одесса необходимо загрузить данные по статьям за последние 5 дней
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        2.
                                    </td>
                                    <td>
                                        далее полученные данные вывести на главной странице вашего сайта в виде таблицы со следующими полями:
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>
                                        * дата публикации в формате (H:i d.m.Y) <br>
                                        * название статьи со ссылкой на ее страницу <br>
                                        * имя автора статьи <br>
                                        * список тегов статьи через запятую <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        3.
                                    </td>
                                    <td>
                                        статьи должны быть отсортированы в алфавитном порядке по имени автора
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        4.
                                    </td>
                                    <td>
                                        должна быть возможность сортировки также по названию и дате публикации
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        5.
                                    </td>
                                    <td>
                                        статьи должны загружаться один раз и сохраняться в базе
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        6.
                                    </td>
                                    <td>
                                        повторная загрузка страницы должна выводить данные из базы
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        7.
                                    </td>
                                    <td>
                                        дополнительно должна быть подготовленна консольная комманда позволяющая обновить данные по статьям
                                    </td>
                                </tr>
                                </tbody>
                                <tfoot></tfoot>
                            </table>
                            <h4>
                                Требования
                            </h4>
                            <table class="table table-hover">
                                <thead>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        Функционал приложения должен быть реализован используя Laravel
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Должны быт использованы принципи ООП, SOLID.
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Код должен содержать достаточное количество комментариев.
                                    </td>
                                </tr>
                                </tbody>
                                <tfoot></tfoot>
                            </table>
                            <h4>
                                В работе должно быть использовано:
                            </h4>
                            <table class="table table-hover">
                                <thead>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        Laravel 6.х;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        MySQL;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        composer для управление пакетами;
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        git для контроля версий в процессе разработки.
                                    </td>
                                </tr>
                                </tbody>
                                <tfoot></tfoot>
                            </table>
                            <h4>
                                Интерфейс
                            </h4>
                            <table class="table table-hover">
                                <thead>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        Требований качеству иинтерфейс нет, но плюсом будет использование Bootstrap или другого фреймворка.
                                    </td>
                                </tr>
                                </tbody>
                                <tfoot></tfoot>
                            </table>
                            <h4>
                                Результат
                            </h4>
                            <table class="table table-hover">
                                <thead>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>
                                        Результатом выполнения задания должен быть проект обладающий как минимум описанным функционалом.
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Процесс инициализации для проверки должен быть описан в файле readme.md.
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Сам проект должен быть расположен в репозитории на bitbucket.org или другом сервисе.'
                                    </td>
                                </tr>
                                </tbody>
                                <tfoot></tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
